/*
Create a file named "assignment9.txt" and open it in write mode.
Write the sentence "UCSC is one of the leading institutes in Sri Lanka for computing studies." to the file.
Open the file in read mode and read the text from the file and print it to the output screen.
Open the file in append mode and append the sentence "UCSC offers undergraduate and postgraduate level courses aiming a range of computing fields." to the file.
*/

#include <stdio.h>

int main(void)
{
    char s[100];
    FILE *ftr;

    printf("\nEnter the Sentence: ");
    scanf("%[^\n]%*c",s);
    ftr = fopen("Assignment9.txt","w");
    fprintf(ftr,"%s",s);
    fclose(ftr);

    ftr = fopen("Assignment9.txt","r");
    int ch;

    while((ch=fgetc(ftr))!=EOF)
    {
        printf("%c",ch);
    }

    fclose(ftr);

    printf("\nEnter the Sentence: ");
    scanf("%[^\n]%*c",s);
    ftr = fopen("Assignment9.txt","a");
    fprintf(ftr,"%s",s);
    fclose(ftr);




}
